deco
====

.. automodule:: commodity.deco
   :members:
   :undoc-members:

args
====

Creating a powerful argument parser is very easy::

  from commodity.args import parser, add_argument, args

  add_argument('-f', '--foo', help='foo argument')
  add_argument('-v', '--verbose', action='store_true', help='be verbose')

  parser.parse_args('--foo 3'.split())

  assert args.foo == 3

And it supports config files. That requires argument specs (``config.specs``)::

  [ui]
  foo = integer(default=0)
  verbose = boolean(default=False)

...the user config file (``.app.config``)::

  [ui]
  foo = 4
  verbose = True

...and the code::

  from commodity.args import parser, add_argument, args

  add_argument('-f', '--foo', help='foo argument')
  add_argument('-v', '--verbose', action='store_true', help='be verbose')

  parser.set_specs("config.specs")
  parser.load_config("/etc/app.config")
  parser.load_config("/home/user/.app.config")
  parser.load_config("by-dir.config")
  parser.parse_args()

  assert args.foo == 4


.. automodule:: commodity.args
   :members:
   :undoc-members:


log
===

.. automodule:: commodity.log
   :members:
   :undoc-members:


mail
====

.. automodule:: commodity.mail
   :members:
   :undoc-members:


os_
===

.. automodule:: commodity.os_
   :members:
   :undoc-members:


path
====

.. automodule:: commodity.path
   :members:
   :undoc-members:


pattern
=======

.. automodule:: commodity.pattern
   :members:
   :undoc-members:


str_
====

.. automodule:: commodity.str_
   :members:
   :undoc-members:


testing
=======

.. automodule:: commodity.testing
   :members:
   :undoc-members:



thread_
=======

.. automodule:: commodity.thread_
   :members:
   :undoc-members:


type_
======

.. automodule:: commodity.type_
   :members:
   :undoc-members:
