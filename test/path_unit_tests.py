#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
from unittest import TestCase

from commodity.path import find_in_ancestors
curdir = os.getcwd()
TOKEN = 'tokenfile'


class PathTest(TestCase):
    def setUp(self):
        os.system('touch %s' % os.path.join(curdir, TOKEN))

    def test_find_in_ancestors_ok(self):
        path = os.path.join(curdir, 'test/util/print.py')
        self.assertEqual(find_in_ancestors(TOKEN, path), curdir)

    def test_find_in_ancestors_fail(self):
        self.assertEqual(find_in_ancestors(TOKEN, '/usr/share/doc'), None)
