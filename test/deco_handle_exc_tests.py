# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase
from commodity.deco import handle_exception
from doublex import Spy, called, assert_that


class Test_handle_exception(TestCase):
    def test_assertion(self):

        spy = Spy()

        @handle_exception(AssertionError, spy.tick)
        def raise_assertion_failed():
            assert False

        raise_assertion_failed()

        assert_that(spy.tick, called())
