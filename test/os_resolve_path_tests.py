# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase

from commodity.os_ import resolve_path


class test_resolve_path(TestCase):
    def test_file_found(self):
        paths = resolve_path('passwd', ['/etc', '/usr/share'])
        assert paths[0] == '/etc/passwd'

    def test_file_not_found(self):
        paths = resolve_path('missing', ['/etc', '/usr/share'])
        assert paths == []

    def test_find_all(self):
        paths = resolve_path('bin', ['/', '/usr'], find_all=True)
        assert paths == ['/bin', '/usr/bin']
