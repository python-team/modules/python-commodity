**commodity** is a set of generic Python utility functions and clases (commodities) for
recurrent operations in almost any program.

[Read the docs](http://commodity.readthedocs.org/)
